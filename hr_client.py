from odoo import models, fields, api


class HrClient(models.Model):
    _name = 'hr.client'
    _description = 'hr_client'


    name = fields.Char(string="Name",required=True)
    last_name = fields.Char(required=True)
    email = fields.Char()
    name_user = fields.Char()


    client_ids = fields.One2many(comodel_name='hr.booking', inverse_name='client_id')