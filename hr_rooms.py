from odoo import models, fields, api


class HrRooms(models.Model):
    _name = 'hr.rooms'
    _description = 'hr_rooms'

    number_beds = fields.Integer()
    tv = fields.Integer()
    bathroom = fields.Integer()
    fridge = fields.Boolean()

    booking_ids = fields.Many2one(comodel_name='hr.booking')





    def tipo_de_suite(number_beds, bathroom):
        if number_beds == 1:
            return "Suite"
        elif number_beds == 2 and bathroom == 2:
            return "Gran Suite"
        elif number_beds == 3:
            return "Suite VIP"
        else:
            return "No se reconoce como suite"

    # Ejemplos de uso
    number_beds = int(input("Ingrese el número de camas: "))
    bathroom = int(input("Ingrese el número de baños: "))

    resultado = tipo_de_suite(number_beds, bathroom)
    print(f"Esta habitación es una {resultado}")
