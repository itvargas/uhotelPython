
from odoo import models, fields, api


class HrBooking(models.Model):
    _name = 'hr.booking'
    _description = 'hr_booking'


    create_date = fields.Date( default=lambda self: self._default_create_date(), copy=False)
    pickup_date = fields.Date(default=True)
    return_date = fields.Date()
    value = fields.Float()

    client_id = fields.Many2one(comodel_name='hr.client')
    rooms_id = fields.Many2one(comodel_name='hr.rooms', inverse_name='booking_id')
    users_id = fields.Many2one(comodel_name='hr.users')
    users_ids = fields.One2many("hr.users", "booking_id", string="users")  

    

  